# repo of utilities

## contents
- getip

### getip
Parses ifconfig and netstat to get IP's and default gateways of all interfaces that have them.

### dtags
Recursive `ctags -R`, drops a `.tag` file in each directory. Designed for code browsing, configure your editor to search for tags local to the file before searching parent directories, this avoids tag collisions that can occure if you're browsing multiple projects in the same editor.

### qclang
Attempts to build and then run a give c(++) file, executable is `${1}.exe`. Passes args through to `clang`.
